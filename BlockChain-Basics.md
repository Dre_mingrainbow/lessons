
## blockchain-basics ##
Let's talk about what a blockchain is and how it operates, and how those operations can help us in our technological needs for generations to come.

### Welcome to the beginning of the #blockchain-change tutorial. ###

# Lesson-One -- What is a blockchain? #

    Section 1 - A    -- An introduction to blockchain.

In this section you will learn the fundamentals of what makes up a blockchain. 

    The basics : 
               The Block

The block is a data structure that stores information the makes up the block.This often includes a block id, a previous hash, a hash, a proof or nonce and transaction data.(edited)
From one chain to another the way the hash is done changes, and the required proof/nonce is handle in a hashing algorithm. Often this will be a while loop that looks for a hash that matches a particular pattern. We will get more into this later. Lets continue, with the basic break down of the blockchain.

```JavaScript
Block {
    id : Number,
    previousHash : String,
    hash : String,
    nonce : Number,
    transactions :  Array
}
```

    Section 1 - B -- Transactions

In this section you will learn the basics of transaction on the blockchain. As each blockchain is different, we will refer to the BlockChange blockchain.  Our own  POW style blockchain.
So, a transaction at the basic level should contain the sender, and receiver wallet addresses, as well as the amount being sent.(edited)

```JavaScript
Transaction {
     id : Unique,
     sender : WalletAddress,
     receiver : WalletAddress,
     signature : SenderSignature
}
```

    Section 1 - C -- The Wallet

In this section we learn about the wallet, and how they work on our chain and other chains.
As we explorer building our very own blockchain we need to consider how the transactions will take place.
We have to consider security, and how we deal with authentication.
For simplicity we will be using RSA Keys without a passphrase, however, in a production grade a passphrase would be optimal for security.(edited)

You can generate a key simple enough, we have integrated RSA Cryptographic functionality into the wallet interface. We are simple using a private (RSA) Key Pair to sign transactional data for later verification using the public version of the RSA Key pair.

You can find it and all of the blockchain change repo's at https://bitbucket.org/BlockchainChange

Blockchain Change Developer
Michael A. Dennis